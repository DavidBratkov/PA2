/*
//David bratkov
//
//CSE224
//
//10/17/2019
//
//PA2
//
//This Progam is basically a game where you try to beat the computer using logic and facts
*/

#include <stdio.h>

char stickgame (int sticknum); //declaring the game as a function

int main(int argc, char **argv) {

int sticks, numbervalue, validnumber;

char next, temp[120], temp2[120];

if ( argc > 2 ) {

printf("Error incorrect input!\n");

return(0);

}

if ( argc == 2) {

        numbervalue=sscanf(argv[1],"%d%s",&sticks,temp2);

        if (( numbervalue != 1) || ( sticks < 10 )) {

                printf("You didnt input a valid integer please try again!\n");

                return(0);

        }

}

if ( argc == 1) {

printf("Hello, and welcome to the Sticks game!\n");

printf("Pick how many sticks you would like to start (10+)\n");

fgets(temp,120,stdin);

numbervalue=sscanf(temp,"%d%s",&sticks,temp2);

//DEBUG-printf("%i",numbervalue);

if ( numbervalue == 1) {

   if ( sticks < 10 ) { //This loop checks if the user didnt type in the correct answer
   printf("That wasnt more than 10 please try again!\n");

   return(0);

   }

   if ( sticks >= 10 ) {

   validnumber=1;

   }

}

if (( numbervalue > 1 ) || ( validnumber != 1 )) {

printf("ERROR INVALID INPUT PLEASE TRY AGAIN!\n");

return(0);

}

}

next=stickgame(sticks);

if ( next == 'u' ) { //The finished game results

printf("Hah I Won!\n");

return(0);

}

if ( next == 'c' ) {

printf("Congradulations you beat me!\n");

return(0);

}

}

char stickgame (int sticknum) { // this is the brains of the game in a function

int pick, i, scanyes;

char next, temp[120], temp2[120];

next='u';

while ( sticknum != 0 )

{

        if ( next == 'u' )   //The logic for the user's turn

        {

                        i=0;

                        while ( i != sticknum )//This is a function that will print out the amount of sticks

                        {

                        printf("|");

                        i=(i+1);

                        }

                printf("(%i)\n",sticknum);




        printf("Pick how much you want to take (1-3)\n");

        fgets(temp,120,stdin);

        scanyes=sscanf(temp,"%i%s",&pick,temp2);

        while ( scanyes != 1 ) {

//      printf("%i",scanyes);

        printf("Incorrect input please try again!\n");

        fgets(temp,120,stdin);

        scanyes=sscanf(temp,"%i%s",&pick,temp2);

        }

                while (( pick != 1 ) && ( pick != 2 ) && ( pick != 3 )) { //Checks to see if the user types it in wrong

                printf("Incorrect choice please try again!\n");

				fgets(temp,120,stdin);

                scanyes=sscanf(temp,"%i%s",&pick,temp2);

                }

        next='c';

        sticknum=(sticknum-pick);

                }

        if (( next == 'c' ) && ( sticknum != 0 )) //The logic for the computer's turn

        {

        i=0;

                        while ( i != sticknum )//This is a function that will print out the amount of sticks

                        {

                        printf("|");

                        i=(i+1);

                        }

                printf("(%i)\n",sticknum);

                if ( sticknum == 3 )

                {

                pick=3;

                }

                if ( sticknum== 2 )

                {

                pick=2;

                }

                if ( sticknum == 1 )

                {

                pick=1;

                }

                if (( sticknum != 3 ) && ( sticknum!= 2 ) && ( sticknum != 1 )) //Logic for when the there is more than 3 sticks left

                {

                pick=(sticknum%4);

                        if ( pick == 0 )

                        {

                        pick=1;

                        }

                }

                if ( pick == 1 ) {

                        printf("The computer takes %i stick\n",pick);

                }

                if ( pick != 1 ) {

        printf("The computer takes %i sticks\n",pick);

                }

                sticknum=(sticknum-pick);

        next='u';

    }

}

return(next);

}

